import { ExecutionContext } from "@nestjs/common";
import { GqlExecutionContext } from "@nestjs/graphql";
import { AuthGuard } from "@nestjs/passport";

/*
In the case of GraphQL, the request context is a bit more explicit and is often accessed through
 the GqlExecutionContext. By using GqlExecutionContext.create(context), you're
 adapting the execution context to the GraphQL context and then extracting the request from it.
*/
export class JwtAuthGuard extends AuthGuard('jwt') {
   getRequest(context: ExecutionContext) {
       const ctx = GqlExecutionContext.create(context)
       return ctx.getContext().req
   }
}