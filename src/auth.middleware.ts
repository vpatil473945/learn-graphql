import { NestMiddleware, UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";

export class AuthMiddleware implements NestMiddleware {
    constructor(private jwtService: JwtService) { }
    use(req: any, res: any, next: (error?: any) => void) {
        const authHeader = req.headers.authorization
        if (authHeader && authHeader.startsWith('Bearer ')) {
            const token = authHeader.slice(7)
            try {
                const decoded = this.jwtService.verify(token)
                req.user = decoded
            } catch (error) {
                throw new UnauthorizedException('Invalid token');
            }
        }
        next()
    }
}