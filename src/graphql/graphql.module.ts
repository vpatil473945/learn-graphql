import { Module } from "@nestjs/common";
import { GraphQLModule } from "@nestjs/graphql";
import { PeopleResolver } from "./people.resolver";
import { ApolloDriver, ApolloDriverConfig } from "@nestjs/apollo"
import { PrismaService } from "src/prisma.service";
import { JwtModule } from "@nestjs/jwt";

@Module({
    imports: [GraphQLModule.forRoot<ApolloDriverConfig>
        ({ driver: ApolloDriver, autoSchemaFile: true, }),
    JwtModule.register({ secret: 'test@Woyce' })],
    providers: [PeopleResolver, PrismaService]
})
export class GraphqlModule { }