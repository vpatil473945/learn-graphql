import { Field, ID, InputType, ObjectType } from "@nestjs/graphql";
/*
@ObjectType() for User:
 Marks the User class as a GraphQL object type. This means instances of this class can be returned in GraphQL queries.

@Field(() => ID) id: number;
Marks the id property as a GraphQL field with a type of ID. The () => ID syntax specifies the type of the field. In GraphQL, ID is often used for unique identifiers.

@Field() name: string; and @Field() email: string;
Mark the name and email properties as GraphQL fields of type String. These fields represent the name and email properties of a user.
*/

@ObjectType()
export class People {
    @Field(() => ID)
    id: String

    @Field()
    email: String

    @Field()
    name: String

    @Field()
    password : String
}

@InputType()
export class createPeopleInput{
    @Field()
    email: String

    @Field()
    name: String

    @Field()
    password : String
}

@InputType()
export class updatePeopleInput{
    @Field(()=> ID)
    id : String

    @Field({nullable:true})
    email?: String

    @Field({nullable:true})
    name?: String

}

@InputType()
export class deletePeople{
    @Field(()=>ID)
    id : String
}

@ObjectType()
export class LoginDone{
    @Field(() => ID)
    id: String

    @Field()
    email: String

    @Field()
    name: String

    @Field()
    token : String
}

@InputType()
export class userLogin{
    @Field()
    email: String

    @Field()
    password : String
}


