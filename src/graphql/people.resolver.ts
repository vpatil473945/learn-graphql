import { Args, Mutation, Query, Resolver } from "@nestjs/graphql";
import { LoginDone, People, createPeopleInput, deletePeople, updatePeopleInput, userLogin } from "./graphql.schema";
import { PrismaService } from "src/prisma.service";
import { HttpException, HttpStatus, UseGuards } from "@nestjs/common";
import { JwtAuthGuard } from "src/jwt-auth.guard";
import { JwtService } from "@nestjs/jwt";
/*
The @Resolver(() => People) decorator indicates that this class is a resolver for the People type. 
It specifies the GraphQL type for which this resolver provides query and mutation functionality.

The GraphQL schema defines the types and operations that clients can perform. However, 
it doesn't specify how to fetch or manipulate the data. 
Resolvers bridge this gap by providing the actual implementation for retrieving or modifying the data.

GraphQL queries and mutations define the operations clients can perform. Resolvers contain the logic for 
executing these operations.
 Each resolver corresponds to a specific query or mutation, specifying how to retrieve or update the data.
*/
@Resolver(() => People)
export class PeopleResolver {
    constructor(private prismaService: PrismaService, private jwtService: JwtService) { }


    /*
        The @Query(() => [People]) decorator defines a GraphQL query named getPeople that returns an array of People objects.
        The resolver function async users(): Promise<User[]> uses the PrismaService 
        to fetch all peoples from the database using findMany().
    */
    @Query(() => [People])
    async getPeople(): Promise<People[]> {
        const allUsers = await this.prismaService.prisma.People.findMany()
        return allUsers.length > 0 ? allUsers : []
    }

    @Mutation(() => People)
    async registerPeople(@Args('data') data: createPeopleInput): Promise<People> {
        const isRegisterd = await this.prismaService.prisma.People.create({ data })
        return isRegisterd
    }

    @Mutation(() => People || String)
    async updatePeople(@Args('updatePayload') updatePayload: updatePeopleInput): Promise<People | String> {
        const { id, ...rest } = updatePayload
        const isUserExist = await this.prismaService.prisma.People.findUnique({ where: { id: +id } })
        if (!isUserExist) throw new HttpException('No User Found!', HttpStatus.CONFLICT)
        console.log('user', !isUserExist)
        return await this.prismaService.prisma.People.update({ where: { id: +id }, data: rest })
    }

    @Mutation(() => People)
    @UseGuards(JwtAuthGuard)
    async deletePeople(@Args('remove') remove: deletePeople): Promise<People> {
        const isUserExist = await this.prismaService.prisma.People.findUnique({ where: { id: +remove.id } })
        if (!isUserExist) throw new HttpException('No User Found!', HttpStatus.CONFLICT)
        return await this.prismaService.prisma.People.delete({ where: { id: +remove.id } })
    }

    @Mutation(() => LoginDone)
    async login(@Args('cred') cred: userLogin): Promise<LoginDone> {
        const { email, password } = cred
        const isAvailable = await this.prismaService.prisma.People.findUnique({ where: { email } })
        if (isAvailable && (password === isAvailable.password)) {
            const { id, name, email } = isAvailable
            const token = this.jwtService.sign({ id, name, email })
            isAvailable.token = token
            return isAvailable
        } else {
            throw new HttpException('email or password invalid!', HttpStatus.CONFLICT)

        }
        throw new HttpException('Something went wrong!', HttpStatus.CONFLICT)
    }
}