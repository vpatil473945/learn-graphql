import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GraphqlModule } from './graphql/graphql.module';
import { JwtModule } from '@nestjs/jwt'
import { JwtStrategy } from './jwt.strategy';
import { JwtAuthGuard } from './jwt-auth.guard';

@Module({
  imports: [JwtModule.register({ secret: 'test@Woyce' }), GraphqlModule],
  controllers: [AppController],
  providers: [AppService , JwtStrategy , JwtAuthGuard],
})
export class AppModule {}
 
